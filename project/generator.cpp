#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
#define pii pair<int,int>
#define pll pair<ll,ll>
#define pdd pair<double,double>
#define X first
#define Y second
#define REP(i,a) for(int i=0;i<a;++i)
#define REPP(i,a,b) for(int i=a;i<b;++i)
#define FILL(a,x) memset(a,x,sizeof(a))
#define foreach( gg,itit )  for( typeof(gg.begin()) itit=gg.begin();itit!=gg.end();itit++ )
#define mp make_pair
#define pb push_back
#define all(s) s.begin(),s.end()
#define present(c,x) ((c).find(x) != (c).end())
const double EPS = 1e-8;
const int mod = 1e9+7;
const int N = 1e6+10;
const ll INF = 1e18;

//#define DEBUG

#ifdef DEBUG
#define dprintf(fmt,...) fprintf(stderr,fmt,__VA_ARGS__)
#else
#define dprintf(fmt,...)
#endif



// end of definitions
const int MAXN = 5000;// max total garbage bins
const int MAXK = 30; //max  total depots
const int MAXC = 100; //max cost per unit distance
const int MAXD = 30;  //max total disposal points 
const int Cord = 10000; // max absolute value of x,y coordinate
const int TYP1 = 50;
const int TYP2 = 10;
vector<pii> loc;
set<pii> taken;
pii rndpoint(){
  int x = rand()%(2*Cord) -Cord;
  int y = rand()%(2*Cord) -Cord;
  if(taken.count(mp(x,y))) return rndpoint();
  taken.insert(mp(x,y)); 
  return mp(x,y);
}

int main(){
  srand(time(NULL));
  freopen("input6","w",stdout);
  
  int cst = rand()%100000,ex = rand()%100000;

  printf("type1 truck Capacity = %d Cost = %d\n",100,cst);
  printf("type2 truck Capacity = %d Cost = %d\n",1000,5*cst+ex);
  int C = rand()%MAXC+1;
  printf("\nTransportation cost per unit distance = %d\n",C);
  int K = rand()%MAXK+1,M=0;
  printf("\nNo. of depots = %d\n",K);
  printf("S.No.   Location    Type1_trucks Type2_trucks\n");
  int tot = 0;
  REP(i,K){
    pii z = rndpoint();
    int typ1 = rand()%TYP1+1,typ2 = rand()%TYP2+1;
    if(typ2>typ1) swap(typ1,typ2);
    printf("%d.    ( %d , %d )    %d        %d\n",i+1,z.X,z.Y,typ1,typ2);
    M+=typ1+typ2;
    tot+=typ2*10+typ1;
  }
  int D = rand()%MAXD+1;
  printf("\nNo. of Disposal points = %d\n",D);
  printf("S.No.   Location\n");
  REP(i,D){
    pii z = rndpoint();
    printf("%d.   ( %d , %d )\n",i+1,z.X,z.Y);
  }
  int n ;

  n=rand()%MAXN+1; 
  while(n>tot/2 || n<tot/8){
    n=rand()%MAXN+1;
  }
  printf("\nNumber of Garbage Bins : %d\n",n);
  printf("S.No.    locations     Garbage Amount\n");
  REP(i,n){
    pii z = rndpoint();
    loc.pb(z);
    int w = rand()%100+1;
    printf("%d.    ( %d , %d )    %d\n",i+1,z.X,z.Y,w);
  }  
  return 0;
} 