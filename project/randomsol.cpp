#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
#define pii pair<int,int>
#define pll pair<ll,ll>
#define pdd pair<double,double>
#define X first
#define Y second
#define REP(i,a) for(int i=0;i<a;++i)
#define REPP(i,a,b) for(int i=a;i<b;++i)
#define FILL(a,x) memset(a,x,sizeof(a))
#define foreach( gg,itit )  for( typeof(gg.begin()) itit=gg.begin();itit!=gg.end();itit++ )
#define mp make_pair
#define pb push_back
#define all(s) s.begin(),s.end()
#define present(c,x) ((c).find(x) != (c).end())
const double EPS = 1e-8;
const int mod = 1e9+7;
const int N = 1e6+10;
const ll INF = 1e15;
#define sqr(x) (x)*(x)
//#define DEBUG

#ifdef DEBUG
#define dprintf(fmt,...) fprintf(stderr,fmt,__VA_ARGS__)
#else
#define dprintf(fmt,...)
#endif

#define MAXN  500
#define TIMES 100

int getno(){
   string s;
   while((cin>>s)){
      if(s.size()==0) return 0;
      int z = 1;
      int i=0;
      if(s[0]=='-') z=-1,i++;
      int x = 0,fl = 0;
      for(;i<s.size();i++) {
        if(s[i]>='0'&&s[i]<='9')
          x=x*10+s[i]-'0';
        else fl=1;
      }
      if(fl) continue;
      return x*z;   
      
   }
}
pii operator + (pii x,pii y){
  return mp(x.X+y.X,x.Y+y.Y);
}

pii operator - (pii x,pii y){
  return mp(x.X-y.X,x.Y-y.Y);
}
double dist(pii x,pii y){
  x=x-y;
  return sqrt(sqr(x.X)+sqr(x.Y));
}

struct dep{
  pii loc ;
  int typ1,typ2,id; 
  void print(){
    printf("Depot: location (%d,%d) typ1 = %d typ2 = %d id = %d\n",loc.X,loc.Y,typ1,typ2,id);
  }
} ;
struct disp{
  pii loc;
  int id;
  void print(){
    printf("Disposal: location (%d,%d) id = %d\n",loc.X,loc.Y,id);
  }
} ;
struct garb{
  pii loc;
  int amt,id;
  void print(){
    printf("Bin: location (%d,%d) amt = %d id = %d\n",loc.X,loc.Y,amt,id);
  }
} ;

typedef vector<struct dep> Cand;
typedef struct garb Bin;
vector<struct dep> deports;
vector<struct disp> disposal;
vector<struct garb> bins;
int cap1,cap2,cost1,cost2, CstKm;
double cost(pii x,pii y){
  return CstKm * dist(x,y);
}
struct pth {
  vector<int> binids;
  int disid,depid,amt;
  double cst;
  int trucktyp;
  void print(){
    printf("Cost of path = %.2lf\nAmount of garbage collected on path = %d\n",cst,amt);
    printf("Truck starts from Depot = %d\n",depid+1);    
    printf("Truck type = %d\n",trucktyp);
    printf("Bins covered in order - ");
    REP(i,binids.size()) printf("%d ",binids[i]+1);
    printf("Disposal Point = %d\n",disid+1);
  }
};

typedef struct pth path;

vector<path> paths;

double fixedcost(struct dep deport){
  return cost1*deport.typ1+cost2*deport.typ2;
}
int istyp2need(int amt){
  return amt>cap1;
}
int getemptytruck(Cand deports,pii &x){
  int s  = 0;
  REP(i,deports.size()) s+=deports[i].typ1+deports[i].typ2;
  if(s==0) return 0;
  while(1){
    x.X=rand()%deports.size(); x.Y=rand()%2+1;
    if(x.Y==1){
      if(deports[x.X].typ1) return 1;
    }else{
      if(deports[x.X].typ2) return 1;
    }
  }
  return 0;
}
double solver(Cand deports){
  double totcost  = 0;
  vector<int> bid ;
  REP(i,bins.size()) bid.pb(i);
  while(bid.size()){

    pii x; 
    if(!getemptytruck(deports,x)) return INF;
    int cap = cap1;
    double cst = 0;
    if(x.Y==2) cap = cap2,deports[x.X].typ2--,cst = cost2; 
    else cap=cap1,deports[x.X].typ1--,cst = cost1;
    
    int fl=100;
    pii loc = deports[x.X].loc;
    
    while(fl&&bid.size()){
      int bt = rand()%bid.size();
      int binps = bid[bt];
      if(cap>=bins[binps].amt){
        cst += cost(loc,bins[binps].loc);
        loc = bins[binps].loc;
        cap -= bins[binps].amt;
        bid.erase(bid.begin()+bt);
      }else fl--;
    }
    int disps = rand()%disposal.size();
    cst += cost(loc,disposal[disps].loc);
    cst += cost(disposal[disps].loc,deports[x.X].loc);

    totcost += cst;  
  }  
  return totcost;
}

void getInput(){
  cap1 = getno(); cost1 = getno();
  cap2 = getno(); cost2 = getno();
  CstKm = getno();
  int K = getno();
  REP(i,K){
    int x=getno(),y=getno();
    dep depo ;
    depo.loc = mp(x,y);
    depo.typ1 = getno();
    depo.typ2 = getno();
    depo.id = i;
    deports.pb(depo);
  //  depo.print();
  }
  int D = getno();
  REP(i,D){
    int x=getno(),y=getno();
    disp dip ;
    dip.loc = mp(x,y);
    dip.id = i;
    disposal.pb(dip);
 //   dip.print();
  }
  int N = getno();
  REP(i,N){
    int x=getno(),y=getno();
    garb gag ;
    gag.loc = mp(x,y);
    gag.amt = getno();
    gag.id = i;
    bins.pb(gag);
 //   gag.print();
  }
}


int main(){
  srand(time(NULL));
  getInput();
  int times = 0;
  double ans = INF;
  while(times<TIMES){
    times++;
    double curr = solver(deports);
    //printf("%lf\n",curr);
    if(curr < ans){
      ans = curr;
    }
  }
  assert(ans< INF-5);
  printf("Final Best overall cost = %.2lf.\n",ans);
  
  
  return 0;
}


