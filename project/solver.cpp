#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
#define pii pair<int,int>
#define pll pair<ll,ll>
#define pdd pair<double,double>
#define X first
#define Y second
#define REP(i,a) for(int i=0;i<a;++i)
#define REPP(i,a,b) for(int i=a;i<b;++i)
#define FILL(a,x) memset(a,x,sizeof(a))
#define foreach( gg,itit )  for( typeof(gg.begin()) itit=gg.begin();itit!=gg.end();itit++ )
#define mp make_pair
#define pb push_back
#define all(s) s.begin(),s.end()
#define present(c,x) ((c).find(x) != (c).end())
const double EPS = 1e-8;
const int mod = 1e9+7;
const int N = 1e6+10;
const ll INF = 1e15;
#define sqr(x) (x)*(x)
//#define DEBUG

#ifdef DEBUG
#define dprintf(fmt,...) fprintf(stderr,fmt,__VA_ARGS__)
#else
#define dprintf(fmt,...)
#endif

#define MAXN  500      // atmost these many bins can be handled , can increase later
#define PATH_PRINT 1   // for printing the final paths 
#define EXTRA_STATS 0  // for printing extra stats i.e, cost at end of each iteration of neighbouring heuristic

int getno(){
   string s;
   while((cin>>s)){
      if(s.size()==0) return 0;
      int z = 1;
      int i=0;
      if(s[0]=='-') z=-1,i++;
      int x = 0,fl = 0;
      for(;i<s.size();i++) {
        if(s[i]>='0'&&s[i]<='9')
          x=x*10+s[i]-'0';
        else fl=1;
      }
      if(fl) continue;
      return x*z;   
      
   }
}
pii operator + (pii x,pii y){
  return mp(x.X+y.X,x.Y+y.Y);
}

pii operator - (pii x,pii y){
  return mp(x.X-y.X,x.Y-y.Y);
}
double dist(pii x,pii y){
  x=x-y;
  return sqrt(sqr(x.X)+sqr(x.Y));
}

struct dep{
  pii loc ;
  int typ1,typ2,id; 
  void print(){
    printf("Depot: location (%d,%d) typ1 = %d typ2 = %d id = %d\n",loc.X,loc.Y,typ1,typ2,id);
  }
} ;
struct disp{
  pii loc;
  int id;
  void print(){
    printf("Disposal: location (%d,%d) id = %d\n",loc.X,loc.Y,id);
  }
} ;
struct garb{
  pii loc;
  int amt,id;
  void print(){
    printf("Bin: location (%d,%d) amt = %d id = %d\n",loc.X,loc.Y,amt,id);
  }
} ;

typedef vector<struct dep> Cand;
typedef struct garb Bin;
vector<struct dep> deports;
vector<struct disp> disposal;
vector<struct garb> bins;
int cap1,cap2,cost1,cost2, CstKm;
double cost(pii x,pii y){
  return CstKm * dist(x,y);
}
struct pth {
  vector<int> binids;
  int disid,depid,amt;
  double cst;
  int trucktyp;
  void print(){
    printf("Cost of path = %.2lf\nAmount of garbage collected on path = %d\n",cst,amt);
    printf("Truck starts from Depot = %d\n",depid+1);    
    printf("Truck type = %d\n",trucktyp);
    printf("Bins covered in order - ");
    REP(i,binids.size()) printf("%d ",binids[i]+1);
    printf("\nDisposal Point = %d\n",disid+1);
  }
};

typedef struct pth path;

vector<path> paths;

double fixedcost(struct dep deport){
  return cost1*deport.typ1+cost2*deport.typ2;
}
int istyp2need(int amt){
  return amt>cap1;
}
bool mergepath(path &a,path &b,path &c){
  if(a.amt+b.amt>cap2) return 0;
  assert(a.depid==b.depid);
  
  c.amt = a.amt+b.amt;
  c.binids = a.binids;
  REP(i,b.binids.size()) c.binids.pb(b.binids[i]);
  c.depid = a.depid;
  c.disid = b.disid;
  c.trucktyp = istyp2need(c.amt)+1;

  c.cst = cost(deports[c.depid].loc,bins[c.binids[0]].loc);
  REP(i,c.binids.size()-1) c.cst += cost(bins[c.binids[i]].loc,bins[c.binids[i+1]].loc);
  c.cst += cost(bins[c.binids.back()].loc,disposal[c.disid].loc);
  c.cst += cost(disposal[c.disid].loc,deports[c.depid].loc);

  return 1;
}
double getBestTransCost(vector<path> &paths,struct dep deport){
  int x = -1,y=-1;double best=INF,sum=0;
    
  REP(i,paths.size()) sum += paths[i].cst;
  path nw;
  REP(i,paths.size()) REP(j,paths.size()) if(i!=j&&mergepath(paths[i],paths[j],nw)){
    double nwcst = sum - paths[i].cst - paths[j].cst + nw.cst;
    int ex = istyp2need(nw.amt) - istyp2need(paths[i].amt) - istyp2need(paths[j].amt);
    if(ex>0){
      if(deport.typ2==0) continue;
    }
    if(nwcst<best){
      x = i, y = j;
      best = nwcst ;
    }
  }
  int valid = 1;
  int tot = 0;    
  REP(i,paths.size()){
    if(!istyp2need(paths[i].amt)) tot++;
  }
  if(tot>deport.typ1+deport.typ2){
    valid = 0;
  }

  if(x==-1|| (valid && best >= sum)){    
    if(!valid) return INF;
    REP(i,paths.size()) if(!istyp2need(paths[i].amt)){
      if(deport.typ1==0) paths[i].trucktyp = 2;
      else deport.typ1--;
    }
    return sum;
  }

  path a = paths[x],b = paths[y];
  vector<path> temp;
  REP(i,paths.size()) if(i!=x&&i!=y) temp.pb(paths[i]);
  mergepath(a,b,nw);
  int ex  = istyp2need(nw.amt) - istyp2need(a.amt) - istyp2need(b.amt);
  deport.typ2 -= ex;
  temp.pb(nw);
  paths=temp;
  return getBestTransCost(paths,deport);
  
}

double solver(Cand deports,int save = 0){
  vector<pair<double,pair<pii,int> > > ord;
  int lim[MAXN],taken[MAXN],used[MAXN];
  REP(i,deports.size()){
    lim[i]=deports[i].typ1*cap1+deports[i].typ2*cap2;
    taken[i]=0;
  }
  FILL(used,0);
  REP(i,bins.size()) REP(j,deports.size()) REP(k,disposal.size()){
    double cst = cost(deports[j].loc,bins[i].loc) + cost(bins[i].loc,disposal[k].loc) + cost(disposal[k].loc,deports[j].loc);
    ord.pb(mp(cst,mp(mp(i,j),k)));
  }
  sort(all(ord));
  int  undn = bins.size();
  vector<path> asgd[MAXN];
  REP(i,ord.size()){
    if(!undn) break;
    int binid = ord[i].Y.X.X,depid = ord[i].Y.X.Y,disid = ord[i].Y.Y;
    if(used[binid]) continue;
    if(lim[depid] < taken[depid] + bins[binid].amt) continue;
    //good to go to use this depot
    used[binid] = 1;
    taken[depid] += bins[binid].amt;
    path npth;
    npth.binids.pb(binid);
    npth.disid = disid;
    npth.depid = depid;
    npth.amt = bins[binid].amt;
    npth.trucktyp = 1;
    npth.cst = ord[i].X;
    asgd[depid].pb(npth);
    undn--;
  }
  if(undn) return INF;
  double cost = 0;
  REP(i,deports.size()){

    double cst = fixedcost(deports[i])+getBestTransCost(asgd[i],deports[i]);
    if(cst>=INF) return INF;

    cost += cst;
  }
  if(save){
    paths.clear();
    REP(i,deports.size()){
      REP(j,asgd[i].size()) paths.pb(asgd[i][j]);
    }  
  }
  return cost;
}

void getInput(){
  cap1 = getno(); cost1 = getno();
  cap2 = getno(); cost2 = getno();
  CstKm = getno();
  int K = getno();
  REP(i,K){
    int x=getno(),y=getno();
    dep depo ;
    depo.loc = mp(x,y);
    depo.typ1 = getno();
    depo.typ2 = getno();
    depo.id = i;
    deports.pb(depo);
  //  depo.print();
  }
  int D = getno();
  REP(i,D){
    int x=getno(),y=getno();
    disp dip ;
    dip.loc = mp(x,y);
    dip.id = i;
    disposal.pb(dip);
 //   dip.print();
  }
  int N = getno();
  REP(i,N){
    int x=getno(),y=getno();
    garb gag ;
    gag.loc = mp(x,y);
    gag.amt = getno();
    gag.id = i;
    bins.pb(gag);
 //   gag.print();
  }
}

vector<Cand> generate_neighbours(Cand curr){
  vector<Cand> neigh;
  REP(i,curr.size()){
    Cand z = curr ;
    if(z[i].typ1){
      z[i].typ1--;
      neigh.pb(z);
      z[i].typ1++;
    }
    if(z[i].typ2){
      z[i].typ2--;
      neigh.pb(z);
      z[i].typ2++; 
    }
  }
  return neigh;
}

int main(){
  getInput();
  // Applying best neighbour heuristic 
  Cand best = deports;
  double ans = solver(best);
  while(1){
    vector<Cand> candidates = generate_neighbours(best);
    if(EXTRA_STATS){
      printf("Best cost = %.2lf in current interation.\n",ans);
    }        
    double mincost = INF;
    Cand bestCan ;
    REP(i,candidates.size()){
      Cand curr = candidates[i];
      double cst = solver(curr);
      if(cst<mincost) {
        mincost = cst;
        bestCan = curr; 
      }
    }
    if(mincost < ans){
      ans = mincost;
      best = bestCan;
    }else break;
  }
  solver(best,1);
  assert(ans< INF-5);
  printf("Final Best overall cost = %.2lf.\n",ans);
  
  if(PATH_PRINT){
    printf("No. of paths = %d.\n",(int) paths.size());
    REP(i,paths.size()) {
      printf("Path %d. -->\n",i+1);
      paths[i].print();
    }
  }
  return 0;
}


