# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
Problem Statement →
There are N garbage bins located across a region whose garbage has to be emptied by M
trucks stationed at K depots. Each garbage bin contains wi (i = 1… N) <=100 quantity of
garbage and there are two types of trucks stationed in each deport. One type with capacity 100
and cost f 1 and other with capacity 1000 and cost f 2 such that f 1 < f 2 . For each depot there are
M 1j (j=1...K) trucks of type one and M 2j (j=1...K) trucks of type two stationed there such that Σ(M 1j
+M 2j ) = M. There are D disposal units where the truck will empty their garbage collected from
different bins and then return to their respective deports. The cost per Km. is defined as C and
hence the cost of transportation between the deports/locations is defined as D pq = C*(distance
between the locations). The objective is to decide the route of the trucks such that garbage is
collected from all the bins and disposed at minimum overall transportation and hiring costs.

